# Changelog

## 1.1.0 - Moved Dockerfile to separate project

*  Using Docker build image from separate project

## 1.0.0 - Initial version

Demonstrates use of

*  CMake as a build tool
*  modern C++ (C++ 17)
*  Docker images for automated CI builds
   *  But also for local Builds inside a container
