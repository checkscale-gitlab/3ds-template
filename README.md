# 3ds-template

A template for building (home-brew) programs for Nintendo's 3DS console.

Demonstrates use of

*  devkitPro g++ compiler and libraries (devkitarm, libctru, citro3d, citro2s)
*  modern C++ (17)
*  CMake as a build tool
*  Docker build image for automated CI builds
   *  But also for local Builds inside a container

Have fun,<br>
Holger Zahnleiter<br>
2021-04-16

## Goals

This example demonstrates how to compile devkitARM programs for Nintendo 3DS by using CMake, Docker and Modern C++.

Why CMake?
All examples comming with devkitPro are using Make as a build tool - and it works.
However, when using CMake I can use Visual Studio Code.
With its CMake plug-in it can now understand the dependencies, find the compiler and the include paths.
This brings - for example - code completion, type hints and all sorts of amenities.

Why Docker?
This way, I can have automated building and testing in a CI.
Furthermore, I can cross-compile by running the container locally.
Doing so, I do not have to install devkitPro on my machine, if I don't want to.
Also a `Dockerfile` documents how a system is to be set up for building 3DS programs.

Why modern C++?
I personally prefer the OO style of C++ over the procedural style of C.
Furthermore I want to use safe features such as smart pointers and others:

*  Initializer lists
*  Type inference (`auto`) and return type deduction
*  `constexpr` instead of Macros (`#define`)
*  `nullptr`for `NULL`
*  `[[nodiscard]]` for return codes
*  ...

Why clean code?
Furthermore I try to apply clean code principles.
This leads to code that is easier to comprehend and maintain:

*  Breaking big functions into smaller functions
*  Coosing selfexplanatory names
*  Making everything immutable (`const`), if possible
*  Avoid negative logic (`!`)
*  Explicit decision structures
*  Consistently using snake case as this is more common in C++ than camel case (and more readable)
*  ...

## Build the project locally

As a prerequisite it is assumed you have CMake (3.19.4 or newer) and devkitPro (July 2020 or newer) installed.

On the root folder you will find a classic `Makefile`.
Just type `make`.
This will create a `build` folder and have CMake build the target in there.

If it succeeds, then you will find `3ds_template.3dsx` inside `build`.
Copy to your device and launch with Homebrew Launcher or run in an emulator (like Citra).

## Build with Docker (locally or CI)

I provide a Docker image for building locally or CI: https://gitlab.com/hzahnlei/3ds-build-image

## Credits

The CMake integration is an updated version of Lectem's original found at https://github.com/Lectem/3ds-cmake.
The 3D/texture example did not compile.
I assume, the original version does not work with the latest version of devkitPro anymore.
(Last commit was five years ago at the time of this writing.)
So I have modified Lectem's CMake files to compile again.
It now also searches for Citro2D and Citro3D.

The C++ code is based on the original C example found at https://github.com/devkitPro/3ds-examples/tree/master/graphics/gpu/textured_cube.
It has been reworked from C to modern C++ and is now built by CMake instead of Make.

## Disclaimer

"Nintendo" and "Nintendo 3DS" are trademarks of Nintendo (see for example https://trademarks.justia.com/850/80/nintendo-85080097.html).

This project is in no way associated with Nintendo.
It is a personal, free, open source and non-profit project.
